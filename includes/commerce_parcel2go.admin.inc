<?php

/**
 * @file
 * Admin functions for Commerce parcel2go.
 */

/**
 * Builds the admin settings form for configuring parcel2go.
 *
 * @return array
 *   Drupal form for parcel2go settings.
 */
function commerce_parcel2go_settings_form() {
  $form = array();
  $form['auth'] = array(
    '#title' => t('parcel2go web authentication'),
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
  );
  $form['auth']['commerce_parcel2go_key'] = array(
    '#type' => 'textfield',
    '#title' => t('parcel2go API Key'),
    '#default_value' => variable_get('commerce_parcel2go_key'),
  );
  
    $form['auth']['commerce_parcel2go_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('parcel2go WSDL URL'),
    '#default_value' => variable_get('commerce_parcel2go_wsdl','https://v3.api.parcel2go.com/ParcelService.asmx?WSDL'),
  );
  
  
  
  /* no password needed for parcel2go
	$form['auth']['commerce_parcel2go_password'] = array(
    '#type' => 'password',
    '#title' => t('parcel2go Password'),
    '#description' => variable_get('commerce_parcel2go_password') ? t('parcel2go password has been set. Leave blank unless you want to change the saved password.') : NULL,
  );
  */
  
  /* not needed for parcel2go
  $form['auth']['commerce_parcel2go_account_number'] = array(
    '#type' => 'textfield',
    '#title' => t('parcel2go Account Number'),
    '#default_value' => variable_get('commerce_parcel2go_account_number'),
  );
  */
  
  /* not needed for parcel2go
  $form['auth']['commerce_parcel2go_meter_number'] = array(
    '#type' => 'textfield',
    '#title' => t('parcel2go Meter Number'),
    '#default_value' => variable_get('commerce_parcel2go_meter_number'),
  );
  */
  
  /* not needed for parcel2go
  $form['auth']['commerce_parcel2go_request_mode'] = array(
    '#type' => 'radios',
    '#title' => t('parcel2go Request Mode'),
    '#description' => t('If switching to production mode, make sure that you have aquired production credentials from parcel2go. These will be different than you testing credentials.'),
    '#options' => array(
      'testing' => t('Testing'),
      'production' => t('Production'),
    ),
    '#default_value' => variable_get('commerce_parcel2go_request_mode', 'testing'),
  );
  */
  
  $form['origin'] = array(
    '#type' => 'fieldset',
    '#title' => t('Ship From Address'),
    '#collapsible' => TRUE,
  );
  $form['origin']['commerce_parcel2go_company_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Company Name'),
    '#default_value' => variable_get('commerce_parcel2go_company_name'),
  );
  $form['origin']['commerce_parcel2go_address_line_1'] = array(
    '#type' => 'textfield',
    '#title' => t('Address'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_address_line_1'),
  );
  $form['origin']['commerce_parcel2go_address_line_2'] = array(
    '#type' => 'textfield',
    '#title' => t('Address (Line 2)'),
    '#default_value' => variable_get('commerce_parcel2go_address_line_2'),
  );
  $form['origin']['commerce_parcel2go_city'] = array(
    '#type' => 'textfield',
    '#title' => t('City'),
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_city'),
  );
  /* not needed - parcel2go is a uk company and would not support shipping from US
  $form['origin']['commerce_parcel2go_state'] = array(
    '#type' => 'textfield',
    '#title' => t('State or Province'),
    '#description' => t('If shipping from USA or Canada, enter the 2 character abbreviation for the shipping State or Province.'),
    '#default_value' => variable_get('commerce_parcel2go_state'),
    '#size' => 2,
  );
  */
  
  $form['origin']['commerce_parcel2go_postal_code'] = array(
    '#type' => 'textfield',
    '#title' => t('Postal Code'),
    '#description' => t('Enter your postal code.'),
    '#size' => 12,
    '#default_value' => variable_get('commerce_parcel2go_postal_code'),
  );
  $form['origin']['commerce_parcel2go_country_code'] = array(
    '#type' => 'select',
    '#title' => t('Country'),
    '#default_value' => variable_get('commerce_parcel2go_country_code'),
    '#options' => array('' => t('Select Country')) + commerce_parcel2go_serviced_countries(),
    '#required' => TRUE,
  );
  $form['services'] = array(
    '#type' => 'fieldset',
    '#title' => t('Enabled parcel2go Shipping Services'),
    '#collapsible' => TRUE,
  );
  $form['services']['commerce_parcel2go_services'] = array(
    '#type' => 'checkboxes',
    '#options' => commerce_parcel2go_shipping_service_types(),
    '#default_value' => variable_get('commerce_parcel2go_services', array()),
  );
  $form['packaging'] = array(
    '#type' => 'fieldset',
    '#title' => t('parcel2go Packaging'),
    '#collapsible' => TRUE,
  );
  $form['packaging']['commerce_parcel2go_default_package_type'] = array(
    '#type' => 'select',
    '#title' => t('Default package type'),
    '#options' => commerce_parcel2go_package_types(),
    '#default_value' => variable_get('commerce_parcel2go_default_package_type', 'YOUR_PACKAGING'),
  );
  $form['packaging']['default_package_size'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default package size (centimeters)'),
    '#collapsible' => FALSE,
    '#description' => 'parcel2go requires a package size when determining estimates.',
  );
  $form['packaging']['default_package_size']['commerce_parcel2go_default_package_size_length'] = array(
    '#type' => 'textfield',
    '#title' => t('Length'),
    '#size' => 5,
	'#field_suffix' => 'cm',
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_default_package_size_length'),
  );
  $form['packaging']['default_package_size']['commerce_parcel2go_default_package_size_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Width'),
    '#size' => 5,
	'#field_suffix' => 'cm',
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_default_package_size_width'),
  );
  $form['packaging']['default_package_size']['commerce_parcel2go_default_package_size_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Height'),
    '#size' => 5,
	'#field_suffix' => 'cm',
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_default_package_size_height'),
  );
    $form['packaging']['maximum_package_weight']['commerce_parcel2go_default_package_max_weight'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum weight per parcel in kg'),
    '#size' => 5,
	'#field_suffix' => 'kg',
    '#required' => TRUE,
    '#default_value' => variable_get('commerce_parcel2go_default_package_max_weight',20),
  );
  $form['packaging']['default_package_size']['details'] = array(
    '#markup' => 'The package size is used to determine the number of packages
     necessary to create a parcel2go shipping cost estimate. <strong>If products do
     not have physical dimensions and weights associated with them, the
     estimates will not be accurate.</strong> The logic implemented works as:
     <ul>
     <li>Assume each order requires at least one package.</li>
     <li>Use the combined volume of all products in an order to determine the
     number of packages.</li>
     </ul>
     This is a simple calculation that can get close to actual shipping costs.
     More complex logic involving multiple package sizes, weights, and void
     space will be intergrated as soon as possible.',
  );
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other parcel2go Options'),
    '#collapsible' => TRUE,
  );
  
  /* not used with parcel2go
  $form['options']['commerce_parcel2go_rate_service_type'] = array(
    '#type' => 'select',
    '#title' => t('Pricing options'),
    '#description' => t('Select the pricing option to use when requesting a rate quote. Note that discounted rates are only available when sending production requests.'),
    '#options' => array(
      'list' => t('Standard pricing (LIST)'),
      'account' => t('This parcel2go account\'s discounted pricing (ACCOUNT)'),
    ),
    '#default_value' => variable_get('commerce_parcel2go_rate_service_type', 'list'),
  );
  */
  
  /* - not used with parcel2go
  $form['options']['commerce_parcel2go_shipto_residential'] = array(
    '#type' => 'select',
    '#title' => t('Ship to destination type'),
    '#description' => t('Leave this set as residential unless you only ship to commercial addresses.'),
    '#options' => array(
      'residential' => t('Residential'),
      'commercial' => t('Commercial')
    ),
    '#default_value' => variable_get('commerce_parcel2go_shipto_residential', 'residential'),
  );
  */
  /* not used for parcel2go
  $form['options']['commerce_parcel2go_dropoff'] = array(
    '#type' => 'select',
    '#title' => t('Default dropoff/pickup location for your parcel2go shipments'),
    '#options' => commerce_parcel2go_dropoff_types(),
    '#default_value' => variable_get('commerce_parcel2go_dropoff', 'REGULAR_PICKUP'),
  );
  */
  
  /* not used with parcel2go
  $form['options']['commerce_parcel2go_insurance'] = array(
    '#type' => 'checkbox',
    '#title' => t('Include insurance value of shippable line items in parcel2go rate requests'),
    '#default_value' => variable_get('commerce_parcel2go_insurance'),
  );
  */
  
  $form['options']['commerce_parcel2go_show_logo'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display parcel2go Logo next to parcel2go services.'),
    '#default_value' => variable_get('commerce_parcel2go_show_logo', 0),
  );
  $form['options']['commerce_parcel2go_log'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Log the following messages for debugging'),
    '#options' => array(
      'request' => t('API request messages'),
      'response' => t('API response messages'),
    ),
    '#default_value' => variable_get('commerce_parcel2go_log', array()),
  );
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration'),
  );
  return $form;
}

/**
 * Validate handler for parcel2go admin settings form.
 *
 * @param array $form
 *   The parcel2go admin settings form.
 * @param array $form_state
 *   The parcel2go admin settings form state.
 */
function commerce_parcel2go_settings_form_validate($form, $form_state) {
  // Define numeric fields that should be validated.
  $numeric_fields = array(
    'commerce_parcel2go_default_package_size_length' => t('length'),
    'commerce_parcel2go_default_package_size_width' => t('height'),
    'commerce_parcel2go_default_package_size_height' => t('width'),
	'commerce_parcel2go_default_package_max_weight' => t('maximum weight'),
  );

  // Validate any numeric fields within the submitted values.
  foreach ($numeric_fields as $key => $numeric_field) {
    if (!is_numeric($form_state['values'][$key])) {
      form_set_error($numeric_field, t('Values for @field can only be set to positive numeric values.', array('@field' => $numeric_field)));
    }
  }

  /* not needed for parcel2go - but may need if wanting to ship from scotland or wales etc...
  // Validate parcel2go settings that are required from USA and Canada only.
  if (in_array($form_state['values']['commerce_parcel2go_country_code'], array('US', 'CA'))) {

    // Make sure state is provided.
    if (empty($form_state['values']['commerce_parcel2go_state'])) {
      form_set_error('commerce_parcel2go_state', t('State or Province is required when shipping from USA or Canada.'));
    }
    else {

      // Make sure state is 2 characters.
      if (!preg_match('/^[a-z][a-z]$/i', $form_state['values']['commerce_parcel2go_state'])) {
        form_set_error('commerce_parcel2go_state', t('Please enter the 2 character abbreviation for your State or Province'));
      }
    }

    // Make sure postal code is provided.
    if (empty($form_state['values']['commerce_parcel2go_postal_code'])) {
      form_set_error('commerce_parcel2go_postal_code', t('Postal code is required when shipping from USA or Canada.'));
    }
  }
  */
}

/**
 * Submit handler for parcel2go admin settings form.
 *
 * @param array $form
 *   The parcel2go admin settings form.
 * @param array $form_state
 *   The parcel2go admin settings form state.
 */
function commerce_parcel2go_settings_form_submit($form, $form_state) {
  // Check the parcel2go service variable before the form is saved to check if this
  // value has been changed in this form submit.
  $services = variable_get('commerce_parcel2go_services', NULL);

/* not needed for parcel2go
  // Make sure State/Province value is saved as uppercase.
  if (!empty($form_state['values']['commerce_parcel2go_state'])) {
    $form_state['values']['commerce_parcel2go_state'] = drupal_strtoupper($form_state['values']['commerce_parcel2go_state']);
  }
*/
  // Loop through each of the settings fields and save their values.
  foreach (commerce_parcel2go_settings_fields() as $key) {
    if (!empty($form_state['values'][$key])) {
      variable_set($key, $form_state['values'][$key]);
    }
    else {
      // If the value is not set then delete the variable instead of saving
      // a NULL value (unless it's the parcel2go password which is only set when
      // it is updated).
      if ($key != 'commerce_parcel2go_password') {
        variable_del($key);
      }
    }
  }

  // If the selected shipping services have changed with this form submit then
  // clear the shipping services, rules, and menu caches.
  if ($services !== $form_state['values']['commerce_parcel2go_services']) {
    commerce_shipping_services_reset();
    entity_defaults_rebuild();
    rules_clear_cache(TRUE);
    menu_rebuild();
  }

  drupal_set_message(t('The parcel2go configuration options have been saved.'));
}
