<?php

/**
 * @file
 * Handles the SOAP request/response to parcel2go Web Services servers.
 */

/**
 * Function to create parcel2go rate request array.
 *
 * @param object $order
 *   The order object.
 *
 * @return array
 *   The array that is created for getting rates from parcel2go.
 */
function commerce_parcel2go_create_rate_request($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  /* not needed for parcel2go
  ********************************
  $request['TransactionDetail'] = array('CustomerTransactionId' => ' *** Rate Request v13 using Drupal Commerce ***');
  $request['Version'] = array(
    'ServiceId' => 'crs',
    'Major' => '13',
    'Intermediate' => '0',
    'Minor' => '0',
  );
  */
  
  // Load the number of packages and their physical attributes.
  $package_line_items = _commerce_parcel2go_get_package_items($order, $order_wrapper);

  // Make sure that there are packages to be sent in the request.
  if (!empty($package_line_items)) {
    //$request['ReturnTransitAndCommit'] = TRUE;
    //$request['shipment']['DropoffType'] = variable_get('commerce_parcel2go_dropoff', 'REGULAR_PICKUP');
    //$request['shipment']['ShipTimestamp'] = date('c');
    //$request['shipment']['PackagingType'] = variable_get('commerce_parcel2go_default_package_type', 'YOUR_PACKAGING');
    $request['shipment']['CollectionAddress'] = _commerce_parcel2go_get_shipper();
    $request['shipment']['DeliveryAddress'] = _commerce_parcel2go_get_recipient($order, $order_wrapper);
    //$request['shipment']['RateRequestTypes'] = strtoupper(variable_get('commerce_parcel2go_rate_service_type', 'list'));
    //$request['shipment']['PackageDetail'] = 'INDIVIDUAL_PACKAGES';
   	//$request['shipment']['PackageCount'] = count($package_line_items);
    $request['shipment']['Parcels'] = $package_line_items;
	$request['shipment']['TotalValue'] = _commerce_parcel2go_get_ordervalue($order);

    // Return the request.
    return $request;
  }
  else {
    // If there are no shippable packages in the order.
    return FALSE;
  }
}

/**
 * Submits a SOAP request to parcel2go and returns the response object.
 *
 * @param object $request
 *   The order object.
 * @param string $method
 *   The method to invoke when submitting the request.
 * @return object
 *   The response object returned after submitting the SOAP request.
 */
function commerce_parcel2go_submit_soap_request($request, $method = 'GetQuotes') {
  // Add the authentication array to the request.
  $request += _commerce_parcel2go_soap_authentication();

  // Allow other modles the ability to alter the request before sending.
  drupal_alter('commerce_parcel2go_submit_soap_request', $request, $method);

  // Log the API request if specified.
  if (in_array('request', variable_get('commerce_parcel2go_log', array()))) {
    $message = t('Submitting API request to parcel2go');
    watchdog('parcel2go', '@message:<pre>@request</pre>', array('@message' => $message, '@request' => print_r($request, TRUE)));
  }

  // ini_set('soap.wsdl_cache_enabled', '0');

  // Determine if the production or testing WSDL should be used.
  // $request_mode = variable_get('commerce_parcel2go_request_mode', 'testing');

  // Determine the correct wsdl file to use for this method.
  // $wsdl_file = _commerce_parcel2go_soap_wsdl_file($method);

  // Locate the WDSL file for describing the web service.
  // $wsdl = drupal_get_path('module', 'commerce_parcel2go') . '/includes/wsdl-' . $request_mode . '/' . $wsdl_file;
  
  $wsdl = variable_get('commerce_parcel2go_wsdl');

  // attempting the request.
  require_once('nusoap/nusoap.php');
  try {
    // Create the SOAP client.
    $client = new nusoap_client($wsdl, true);
    // Attempt the SOAP request.
    //$response = $client->$method($request);
	
	// Call API 
	$result = $client->call('GetQuotes', $request);
	
	//drupal_set_message('<br><pre>*********************************************<br>'.'</pre>');	
	//drupal_set_message('<hr><br><pre>commerce_parcel2go_submit_soap_request - soap client - $request<br>'. print_r($request, TRUE) .'</pre>');	
	//drupal_set_message('<hr><br><pre>commerce_parcel2go_submit_soap_request - soap client - $client<br>'. print_r($client, TRUE) .'</pre>');	
	//drupal_set_message('<hr><br><pre>commerce_parcel2go_submit_soap_request - soap client - $result<br>'. print_r($result, TRUE) .'</pre>');	
	//drupal_set_message('<hr><br><pre>commerce_parcel2go_submit_soap_request - soap client - $_SESSION<br>'. print_r($_SESSION, TRUE) .'</pre>');	
	//drupal_set_message('<br><pre>*********************************************<br>'.'</pre>');	


	
    // Log the API request if specified.
    if (in_array('response', variable_get('commerce_parcel2go_log', array()))) {
      watchdog('parcel2go', 'API response received:<pre>@result</pre>', array('@result' => print_r($result, TRUE)));
    }
	
	if ($result['GetQuotesResult']['Success']=='false') {
		drupal_set_message('<hr><br><pre>Error obtaining shipping quote: '. print_r($result['GetQuotesResult']['ErrorMessage'], TRUE) .'</pre>');
		return FALSE;
	}
	
    $errors = array('FAILURE', 'false');
    if (!in_array($result['GetQuotesResult']['Success'], $errors)) {		// check to see what is returned
      // Return the response object if there are no fatal errors.
      return $result;
    }
  } catch (SoapFault $exception) {
    watchdog('parcel2go', '<h2>SOAP Fault</h2><br /><b>Code:</b> @faultcode <br /><b>String:</b> @faultstring', array('@faultcode' => $exception->faultcode, '@faultstring' => $exception->faultstring));
  }
  return FALSE;
}

/**
 * Internal function to build authentication array for SOAP request.
 *
 * @return array
 *   An array including all of the authentication values for parcel2go.
 */
function _commerce_parcel2go_soap_authentication() {
  $authentication = array();

  // Add the parcel2go web services key and password credentials.
  $authentication['apiKey'] = variable_get('commerce_parcel2go_key', NULL);
      //'Password' => variable_get('commerce_parcel2go_password', NULL), //password not needed for parcel2go

  // Add the parcel2go web services account and meter numbers.
  /* not needed for parcel2go - was for fedex
  $authentication['ClientDetail'] = array(
    'AccountNumber' => variable_get('commerce_parcel2go_account_number', NULL),
    'MeterNumber' => variable_get('commerce_parcel2go_meter_number', NULL),
  );
  */
  
  return $authentication;
}


// not used by parcel2go
/**
 * Internal function to return an array of wsdl files keyed by their metho
 *
 * @param string $method
 *   The parcel2go API method being called.
 * @return string
 *   The file name that corresponds with the method being called.
 */
 /*not used by parcel2go
function _commerce_parcel2go_soap_wsdl_file($method) {
  // Build an array of wsdl file names that is keyed by parcel2go API methods.
  $files = array(
    'GetQuotes' => 'RateService_v13.wsdl',
  );
  if (!empty($files[$method])) {
    return $files[$method];
  }
  return FALSE;
}
*/

/**
 * Internal function to build shipper (ship from) array.
 *
 * @return array
 *   An array that represents the ship from address.
 */
function _commerce_parcel2go_get_shipper() {
  $shipper = array(
      'PersonName' => variable_get('commerce_parcel2go_company_name'),
      'CompanyName' => variable_get('commerce_parcel2go_company_name'),
      'StreetLines' => array(variable_get('commerce_parcel2go_address_line_1')),
      'City' => variable_get('commerce_parcel2go_city'),
      'StateOrProvinceCode' => variable_get('commerce_parcel2go_state'),
      'PostalCode' => variable_get('commerce_parcel2go_postal_code'),
      'CountryCode' => variable_get('commerce_parcel2go_country_code'),
  );
  return $shipper;
}

/**
 * Internal function to build recipient (ship to) array.
 *
 * @param object $order
 *   The order object.
 * @param object $order_wrapper
 *   The order entity wrapper.
 *
 * @return array
 *   An array that represents the ship to address.
 */
function _commerce_parcel2go_get_recipient($order, $order_wrapper) {
  $field_name = commerce_physical_order_shipping_field_name($order);
  // Prepare the shipping address for use in the request.
  if (!empty($order_wrapper->{$field_name}->commerce_customer_address)) {
    $shipping_address = $order_wrapper->{$field_name}->commerce_customer_address->value();
  }
  else {
    $shipping_address = addressfield_default_values();
  }
  $recipient = array(
      'PersonName' => $shipping_address['name_line'],
      'StreetLines' => array($shipping_address['thoroughfare']),
      'City' => $shipping_address['locality'],
      'PostalCode' => $shipping_address['postal_code'],
      'CountryCode' => $shipping_address['country'],
      'Residential' => variable_get('commerce_parcel2go_shipto_residential', 'residential') == 'residential' ? TRUE : FALSE,
  );

  // StateOrProvinceCode is only required for shipping to U.S. and Canada.
  if (in_array($shipping_address['country'], array('US', 'CA'))) {
    $recipient['Address']['StateOrProvinceCode'] = $shipping_address['administrative_area'];
  }

  return $recipient;
}

/**
 * Internal function to calculate order value
 *
 * @param object $order
 *   The order object.
 *
 * @return value
 *
 */
function _commerce_parcel2go_get_ordervalue($order) {

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
 
  // Retrieve its total amount and currency code.
  $total_amount = $wrapper->commerce_order_total->amount->value() / 100;		// drupal returns the pence value not pound value
  //$total_currency = $wrapper->commerce_order_total->currency_code->value();
	return $total_amount;
}

/**
 * Internal function to determine shippable line items from the order.
 *
 * @param object $order
 *   The commerce order object for the order that we're requesting rates for.
 *
 * @return array
 *   The list of packages and dimensions for this order that should be submitted
 *   to parcel2go.
 */
function _commerce_parcel2go_get_package_items($order) {
  // Get the weight and volume of the shippable items.
  $weight = commerce_physical_order_weight($order, 'kg');
  $volume = commerce_physical_order_volume($order, 'cm');

  // Determine the default package volume from the parcel2go settings.
  $default_package_volume = variable_get('commerce_parcel2go_default_package_size_length', '0') * variable_get('commerce_parcel2go_default_package_size_width', '0') * variable_get('commerce_parcel2go_default_package_size_height', '0');

  // Calculate the number of packages that should be created based on the
  // size of products and the default package volume.
  $number_of_packages = ceil($volume['volume'] / $default_package_volume);

  // Check the parcel2go settings to see if we should request insurance.
  $insurance_value = array();
  if (variable_get('commerce_parcel2go_insurance')) {
    $insurance_value = _commerce_parcel2go_get_insurance_value($order);
  }

  // Loop through the number of caluclated package to create the return array.
  for ($i = 1; $i <= $number_of_packages; $i++) {

	  $package_line_items['Parcel'] = array(
      //'SequenceNumber' => $i,
      //'GroupPackageCount' => 1,
      'Weight' => round(max(array(0.1, $weight['weight'] / $number_of_packages)), 2),	// weight in kg	
      'Length' => variable_get('commerce_parcel2go_default_package_size_length'),		// length in cm
      'Width' => variable_get('commerce_parcel2go_default_package_size_width'),			// width in cm
      'Height' => variable_get('commerce_parcel2go_default_package_size_height'),		// height in cm
      );

    // If an isurance value has been returned, add insurance value to request.
    /*
	if (!empty($insurance_value['amount']) && $insurance_value['amount'] > 0) {
      $package_line_items[$i - 1]['InsuredValue']['Currency'] = $insurance_value['currency_code'];
      $package_line_items[$i - 1]['InsuredValue']['Amount'] = round($insurance_value['amount'] / $number_of_packages, 2);
    }
	*/
  }

  // Make sure that we've found shippable items in the order.
  if (!empty($package_line_items)) {
    return $package_line_items;
  }
  else {
    return FALSE;
  }
}

/**
 * Internal function to caluclate insurance value of shippable line items.
 *
 * @param object $order
 *   The commerce order object for the order that we're requesting rates for.
 *
 * @return array
 *   The total value of shippable items in the order for insurance.
 */
function _commerce_parcel2go_get_insurance_value($order) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_total = $order_wrapper->commerce_order_total->value();
  $insurance_value = 0;

  // Loop over each line item on the order.
  foreach ($order_wrapper->commerce_line_items as $line_item_wrapper) {

    // Only collect value of product line items that are shippable.
    if (in_array($line_item_wrapper->type->value(), commerce_product_line_item_types())
        && commerce_physical_line_item_shippable($line_item_wrapper->value())) {
      $line_item_total = $line_item_wrapper->commerce_total->value();

      // Increment the insurance value from the line items value.
      $insurance_value += commerce_currency_amount_to_decimal($line_item_total['amount'], $line_item_total['currency_code']);
    }
  }

  // Return the insurance value and currency code of shippable items.
  return array(
    'amount' => $insurance_value,
    'currency_code' => $order_total['currency_code'],
  );
}
